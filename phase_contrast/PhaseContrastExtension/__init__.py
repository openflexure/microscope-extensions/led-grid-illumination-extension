import logging
from time import sleep

import numpy as np
from labthings import fields, find_component, find_extension
from labthings.extensions import BaseExtension
from labthings.views import ActionView, PropertyView, View
from openflexure_microscope.api.utilities.gui import build_gui
from openflexure_microscope.captures.capture_manager import generate_basename
from picamerax.array import PiRGBArray
from PIL import Image


class PhaseContrastImageAction(ActionView):
    args = {
        "axis": fields.String(required=True),
        "max_color_red": fields.Integer(missing=0, allow_none=True),
        "max_color_green": fields.Integer(missing=0, allow_none=True),
        "max_color_blue": fields.Integer(missing=0, allow_none=True),
        "min_color_red": fields.Integer(missing=0, allow_none=True),
        "min_color_green": fields.Integer(missing=0, allow_none=True),
        "min_color_blue": fields.Integer(missing=0, allow_none=True),
        "led_height": fields.Number(missing=0, allow_none=True),
        "led_pitch": fields.Number(missing=0, allow_none=True),
        "color_mode": fields.String(required=True),
        "options": fields.List(fields.String, missing=[], allow_none=True),
        "imaging_mode": fields.String(required=True),
    }

    def post(self, args):
        self.axis = args.get("axis")
        self.max_color = np.array(
            [
                args.get("max_color_red") or 0,
                args.get("max_color_green") or 0,
                args.get("max_color_blue") or 0,
            ]
        )
        self.min_color = np.array(
            [
                args.get("min_color_red") or 0,
                args.get("min_color_green") or 0,
                args.get("min_color_blue") or 0,
            ]
        )
        self.color_mode = args.get("color_mode")
        self.options = args.get("options") or {}
        self.imaging_mode = args.get("imaging_mode")
        self.grid_illumination_extension = find_extension(
            "org.openflexure.grid-illumination"
        )
        self.microscope = find_component("org.openflexure.microscope")
        self.led_height = args.get("led_height")
        self.led_pitch = args.get("led_pitch")

        if "Save separate images" in self.options:
            self.save_partial = True
        else:
            self.save_partial = False

        self.old_led_matrix = self.grid_illumination_extension.led_grid.state_matrix

        self.grid_LUT = self.generate_grid_LUT()

        if self.imaging_mode == "2D":
            self.phase_contrast_2d()
        elif self.imaging_mode == "3D":
            self.phase_contrast_3d()
        elif self.imaging_mode == "Fourier ptychography":
            self.fourier_ptychography()

    def generate_grid_LUT(self):
        width = self.grid_illumination_extension.led_grid.width
        height = self.grid_illumination_extension.led_grid.height

        r_mm = (
            np.sqrt(np.power(((width - 1) / 2), 2) + (np.power(((height - 1) / 2), 2)))
            * self.led_pitch
        )
        logging.info(r_mm)

        grid_sphere = np.zeros((width, height), "float")

        for ii in range(width):
            for jj in range(height):
                x_mm = np.abs(ii - (width - 1) / 2) * self.led_pitch
                y_mm = np.abs(jj - (width - 1) / 2) * self.led_pitch
                grid_sphere[ii, jj] = np.sqrt(
                    np.power(r_mm, 2) - np.power(x_mm, 2) - np.power(y_mm, 2)
                )

        grid_sphere = 1 - (grid_sphere - np.amin(grid_sphere)) / np.amax(grid_sphere)

        grid_sphere_red = np.round(
            (grid_sphere * (self.max_color[0] - self.min_color[0]) + self.min_color[0])
        )
        grid_sphere_blue = np.round(
            (grid_sphere * (self.max_color[1] - self.min_color[1]) + self.min_color[1])
        )
        grid_sphere_green = np.round(
            (grid_sphere * (self.max_color[2] - self.min_color[2])) + self.min_color[2]
        )

        grid_LUT = np.dstack((grid_sphere_red, grid_sphere_blue, grid_sphere_green))
        return grid_LUT

    def fourier_ptychography(self):
        capture_time = generate_basename()

        width = self.grid_illumination_extension.led_grid.width
        height = self.grid_illumination_extension.led_grid.height

        with self.microscope.camera.lock, self.microscope.stage.lock:
            for ii in range(width):
                for jj in range(height):
                    self.grid_illumination_extension.led_grid.set_shape(
                        "all", np.array([0, 0, 0]), True
                    )
                    self.grid_illumination_extension.led_grid.set_pixel(
                        ii, jj, self.grid_LUT[ii, jj]
                    )
                    sleep(0.1)
                    image = self.capture(self.microscope.camera).astype(np.float16)
                    if self.color_mode == "greyscale":
                        image = np.average(image, axis=2)
                    if self.save_partial:
                        self.save_image(
                            image,
                            self.color_mode,
                            "Fourier Ptychography/" + capture_time,
                            height * ii + jj,
                        )
                    # sleep(0.4)

            self.grid_illumination_extension.led_grid.set_state_matrix(
                self.old_led_matrix
            )

    def phase_contrast_3d(self):
        capture_time = generate_basename()

        width = self.grid_illumination_extension.led_grid.width
        height = self.grid_illumination_extension.led_grid.height

        if self.axis == "x":
            direction_length = width
            middle_position = np.floor(height / 2).astype("int")
        else:
            direction_length = height
            middle_position = np.floor(width / 2).astype("int")

        with self.microscope.camera.lock, self.microscope.stage.lock:
            for ii in range(direction_length):
                self.grid_illumination_extension.led_grid.set_shape(
                    "all", np.array([0, 0, 0]), True
                )
                if self.axis == "x":
                    self.grid_illumination_extension.led_grid.set_pixel(
                        ii, middle_position, self.grid_LUT[ii, middle_position]
                    )
                else:
                    self.grid_illumination_extension.led_grid.set_pixel(
                        middle_position, ii, self.grid_LUT(middle_position, ii)
                    )
                sleep(0.1)
                image = self.capture(self.microscope.camera).astype(np.float16)
                if self.color_mode == "greyscale":
                    image = np.average(image, axis=2)
                if self.save_partial:
                    self.save_image(
                        image, self.color_mode, "Phase Contrast 3D/" + capture_time, ii
                    )
                # sleep(0.4)

            self.grid_illumination_extension.led_grid.set_state_matrix(
                self.old_led_matrix
            )

    def phase_contrast_2d(self):
        capture_time = generate_basename()

        if self.axis == "x":
            shapes = ["left", "right", "all"]
        else:
            shapes = ["top", "bottom", "all"]

        with self.microscope.camera.lock, self.microscope.stage.lock:
            self.grid_illumination_extension.led_grid.set_shape_LUT(
                shapes[0], self.grid_LUT, True
            )
            sleep(0.1)
            image1 = self.capture(self.microscope.camera).astype(np.float16)
            self.grid_illumination_extension.led_grid.set_shape_LUT(
                shapes[1], self.grid_LUT, True
            )
            sleep(0.1)
            image2 = self.capture(self.microscope.camera).astype(np.float16)
            self.grid_illumination_extension.led_grid.set_shape_LUT(
                shapes[0], self.grid_LUT, False
            )
            sleep(0.1)
            brightfield = self.capture(self.microscope.camera).astype(np.float16)

            # Turn off during processing
            self.grid_illumination_extension.led_grid.set_shape(
                shapes[0], np.array([0, 0, 0]), True
            )
            # some hacky processing needed to reduce memory usage.
            # At full resolution saving and converting to grayscale at the end results in OOM kill

            if self.color_mode == "greyscale":
                image1 = np.average(image1, axis=2)
                image2 = np.average(image2, axis=2)
                brightfield = np.average(brightfield, axis=2)

            if self.save_partial:
                self.save_image(
                    image1, self.color_mode, "Phase Contrast 2D/" + capture_time, "1"
                )
                self.save_image(
                    image2, self.color_mode, "Phase Contrast 2D/" + capture_time, "2"
                )
                self.save_image(
                    brightfield,
                    self.color_mode,
                    "Phase Contrast 2D/" + capture_time,
                    "Brightfield",
                )
            self.grid_illumination_extension.led_grid.set_state_matrix(
                self.old_led_matrix
            )

            image2 -= image1
            image1 = None
            brightfield += 1e-12 * np.ones(brightfield.shape, dtype=np.float16)
            image2 *= 255.0
            image = np.absolute(image2 / brightfield)
            image2 = None
            brightfield = None
            self.save_image(
                image, self.color_mode, "Phase Contrast 2D/" + capture_time, "DPC"
            )

    def capture(self, camera):
        with PiRGBArray(
            camera.picamera, size=tuple(camera.picamera.MAX_RESOLUTION)
        ) as output:
            camera.stop_stream()
            camera.picamera.resolution = tuple(camera.picamera.MAX_RESOLUTION)
            camera.picamera.capture(output, format="rgb")
            camera.start_stream()
            return output.array

    def save_image(self, image_data, color_mode="rgb", folder="", filename=None):
        capture_object = self.microscope.captures.new_image(False, filename, folder)
        image = Image.fromarray(
            image_data.astype(np.uint8), "RGB" if color_mode == "rgb" else "L"
        )
        image.save(capture_object.stream, format="jpeg")
        capture_object.flush()
        # capture_object.put_and_save( #only needed for 2.7, done in flush() in 2.8
        #     metadata={
        #         "image": {
        #             "id": capture_object.id,
        #             "name": capture_object.name,
        #             "acquisitionDate": capture_object.datetime.isoformat(),
        #             "format": capture_object.format,
        #             "tags": capture_object.tags,
        #             "annotations": capture_object.annotations,
        #         }
        #     })


# Create the extension class
class PhaseContrastExtension(BaseExtension):
    def __init__(self):
        super().__init__("org.openflexure.phase-contrast", version="0.0.1")

        def gui_func():
            return {
                "icon": "border_clear",
                "forms": [
                    {
                        "name": "Capture a phase contrast image",
                        "route": "/capture-phase-contrast",
                        "isTask": True,
                        "isCollapsible": True,
                        "submitLabel": "Capture",
                        "schema": [
                            {
                                "fieldType": "selectList",
                                "name": "imaging_mode",
                                "label": "Imaging mode",
                                "value": "2D",
                                "options": ["2D", "3D", "Fourier ptychography"],
                            },
                            {
                                "fieldType": "selectList",
                                "name": "axis",
                                "label": "Axis",
                                "value": "x",
                                "options": ["x", "y"],
                            },
                            {
                                "fieldType": "selectList",
                                "name": "color_mode",
                                "label": "Capture colour mode",
                                "value": "greyscale",
                                "options": ["greyscale", "rgb"],
                            },
                            [
                                {
                                    "fieldType": "numberInput",
                                    "name": "max_color_red",
                                    "label": "Max Red",
                                    "min": 0,
                                    "default": 0,
                                    "max": 255,
                                },
                                {
                                    "fieldType": "numberInput",
                                    "name": "max_color_green",
                                    "label": "Max Green",
                                    "min": 0,
                                    "default": 0,
                                    "max": 255,
                                },
                                {
                                    "fieldType": "numberInput",
                                    "name": "max_color_blue",
                                    "label": "Max Blue",
                                    "min": 0,
                                    "default": 0,
                                    "max": 255,
                                },
                            ],
                            [
                                {
                                    "fieldType": "numberInput",
                                    "name": "min_color_red",
                                    "label": "Min Red",
                                    "min": 0,
                                    "default": 0,
                                    "max": 255,
                                },
                                {
                                    "fieldType": "numberInput",
                                    "name": "min_color_green",
                                    "label": "Min Green",
                                    "min": 0,
                                    "default": 0,
                                    "max": 255,
                                },
                                {
                                    "fieldType": "numberInput",
                                    "name": "min_color_blue",
                                    "label": "Min Blue",
                                    "min": 0,
                                    "default": 0,
                                    "max": 255,
                                },
                            ],
                            {
                                "fieldType": "numberInput",
                                "name": "led_height",
                                "label": "Height of LED grid above sample (mm)",
                                "min": 0,
                                "default": 5,
                            },
                            {
                                "fieldType": "numberInput",
                                "name": "led_pitch",
                                "label": "LED pitch",
                                "min": 0,
                                "default": 3.187,
                            },
                            {
                                "fieldType": "checkList",
                                "name": "options",
                                "label": "Options",
                                "value": [],
                                "options": ["Save separate images"],
                            },
                        ],
                    }
                ],
            }

        self.add_view(PhaseContrastImageAction, "/capture-phase-contrast")
        self.add_meta("gui", build_gui(gui_func, self))
