# LED Grid illumination extensions

## Description
Extensions for experiments with [DotStar Grid](https://www.adafruit.com/product/3444) for adjustable illumination. The grid illumination extension allows control of the grid through the UI, the phase contrast extension can be used to obtain images with different illumination patters to be subsequently processed to get phase data (the PI is not powerful enough to do much processing directly).

## How to use
To use the dotstar grid you will need the dotstar grid and a level shifter (the grid works at 5V and may not detect 3.3V signals from the pi). The wiring is very simple, you can follow [this documentation](https://build.openflexure.org/openflexure-delta-stage/v1.2.0/pages/LED_grid_illumination.html).

On the software side, you need to enable SPI on the pi (e.g. through `raspi-config`). You may need to change the installation folder permissions to allow you to install the required libraries:

```
sudo chmod -R g+w /var/openflexure/
sudo adduser pi openflexure-ws
```

Activate the OFM virtual env so we can install libraries within it
```
ofm activate
```

The grid_illumination extension requires adafruit-circuitpython-dotstar, phase_contrast extension requires Pillow, you can install both using:
```
pip install adafruit-circuitpython-dotstar Pillow
```

If you only want to use the grid\_illumination extension you do not need to install `Pillow` and you can skip commands involving the phase\_contrast extension below.


As this repo contains two separate extensions, you will need to move or link the extension directories into OFM extension directory root. You can either simply copy the folders to `/var/openflexure/extensions/microscope_extensions/` or you can symlinks:

```
git clone https://gitlab.com/openflexure/microscope-extensions/led-grid-illumination-extension.git; cd led-grid-illumination-extension; #skip if you already downloaded the extension
ln -s `realpath grid_illumination` /var/openflexure/extensions/microscope_extensions/
ln -s `realpath phase_contrast` /var/openflexure/extensions/microscope_extensions/
```

Restart OFM to reload extensions

```
ofm restart
```
You should now see the extensions in the interface. If you followed these instructions and you don't see the extension please report a bug in this repo.
