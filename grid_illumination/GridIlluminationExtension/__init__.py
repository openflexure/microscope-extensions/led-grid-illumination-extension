import logging
from time import time

from labthings import find_component
from labthings.extensions import BaseExtension
from labthings.utilities import path_relative_to
from openflexure_microscope.api.utilities.gui import build_gui

from .GridIlluminationView import *
from .LedGrids.DotStarGrid import DotStarGrid
from .LedGrids.LedGrid import LedGrid


# Create the extension class
class GridIlluminationExtension(BaseExtension):
    def __init__(self):
        super().__init__(
            "org.openflexure.grid-illumination",
            version="0.0.1",
            static_folder=path_relative_to(__file__, "static"),
        )

        self.led_grid = DotStarGrid()
        self.color_picker = dict(red=0, blue=0, green=0)

        def gui_func():
            return {
                "icon": "highlight",
                "frame": {
                    "href": f"api/v2/extensions/org.openflexure.grid-illumination/get-illumination-matrix-form?{time()}"
                },
            }

        self.add_view(GridIlluminationSetShapeView, "/set-illumination-shape")
        self.add_view(GridIlluminationMatrixView, "/get-illumination-matrix-form")
        self.add_meta("gui", build_gui(gui_func, self))

        self.script_commands = {
            "ledShape": {
                "function": GridIlluminationExtension.led_shape,
                "description": "Set LED grid shape. Supported shapes are "
                + ",".join(self.led_grid.supported_shapes.keys()),
                "args": [
                    "str:Shape name",
                    "int:Red (0-255)",
                    "int:Green (0-255)",
                    "int:Blue (0-255)",
                ],
                "kwargs": {"add": "?int:Use add mode (0/1), default 0"},
                "locks": [],
            }
        }

    def save_color_picker(self, red, green, blue):
        self.color_picker["red"] = red
        self.color_picker["green"] = green
        self.color_picker["blue"] = blue

    def led_shape(self, shape, r, g, b, **kwargs):
        logging.info(kwargs)
        self.led_grid.set_shape(
            shape,
            np.array([int(r), int(g), int(b)]),
            "add" not in kwargs or not kwargs["add"] == "1",
        )
        return True
