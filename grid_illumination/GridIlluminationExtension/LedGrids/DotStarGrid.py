import board
import numpy as np
from adafruit_dotstar import DotStar

from .LedGrid import LedGrid


class DotStarGrid(LedGrid):
    def __init__(self, width=8, height=8, offset=[[0, 0], [0, 0]]):
        offset = np.array(offset)
        self.offset_y_start = offset[0, 0]
        self.offset_x_start = offset[0, 1]

        self.array_width = width + offset[:, 1].sum()
        self.array_height = height + offset[:, 0].sum()
        self.dotstar = DotStar(
            board.SCK,
            board.MOSI,
            self.array_width * self.array_height,
            auto_write=False,
        )
        for i in range(self.array_width * self.array_height):
            self.dotstar[i] = (0, 0, 0)
        super().__init__(width, height, offset)

    def write_to_led(self):
        for y in range(self.height):
            for x in range(self.width):
                self.dotstar[
                    (y + self.offset_y_start) * self.array_width
                    + x
                    + self.offset_x_start
                ] = self.state_matrix[y, x]
        self.dotstar.show()
