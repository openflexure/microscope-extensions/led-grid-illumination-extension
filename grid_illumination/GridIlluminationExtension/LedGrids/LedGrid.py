import numpy as np


class LedGrid:
    supported_shapes = {
        "all": [],
        "right": [],
        "left": [],
        "bottom": [],
        "top": [],
        "center": [],
        "centerCol": [],
        "centerRow": [],
        "centerCross": [],
        "outerRing": ["Width - how many leds from the edge should be included"],
    }
    state_matrix = []

    def __init__(self, width=8, height=8, offset=[[0, 0], [0, 0]]):
        self.width = width
        self.height = height
        self.offset = offset
        self._init_shapes()
        self.set_shape("all", [0, 0, 0], True)

    def _init_shapes(self):
        shapes = {}
        width = self.width
        height = self.height

        shapes["all"] = np.ones([height, width])
        widthOdd = width % 2
        heightOdd = height % 2

        shapes["right"] = np.concatenate(
            (np.zeros([height, width // 2 + widthOdd]), np.ones([height, width // 2])),
            axis=1,
        )
        shapes["left"] = np.concatenate(
            (np.ones([height, width // 2]), np.zeros([height, width // 2 + widthOdd])),
            axis=1,
        )
        shapes["bottom"] = np.concatenate(
            (np.zeros([height // 2 + heightOdd, width]), np.ones([height // 2, width]))
        )
        shapes["top"] = np.concatenate(
            (np.ones([height // 2, width]), np.zeros([height // 2 + heightOdd, width]))
        )
        shapes["center"] = np.zeros([height, width])
        shapes["center"][
            np.ceil(height / 2).astype("int")
            - 1 : np.ceil(height / 2).astype("int")
            + (1 - heightOdd),
            np.ceil(width / 2).astype("int")
            - 1 : np.ceil(width / 2).astype("int")
            + (1 - widthOdd),
        ] = (
            1
        )  # if array dimension is odd will return 1 in the center, if it is even will return the two either side of center.
        if widthOdd:
            shapes["centerCol"] = shapes["all"] - shapes["left"] - shapes["right"]
        else:
            shapes["centerCol"] = np.pad(
                np.concatenate(
                    (np.zeros([height, width // 2 - 1]), np.ones([height, 2])), axis=1
                ),
                ((0, 0), (0, width // 2 - 1)),
                constant_values=0,
            )

        if heightOdd:
            shapes["centerRow"] = shapes["all"] - shapes["top"] - shapes["bottom"]
        else:
            shapes["centerRow"] = np.pad(
                np.concatenate(
                    (np.zeros([height // 2 - 1, width]), np.ones([2, width]))
                ),
                ((0, height // 2 - 1), (0, 0)),
                constant_values=0,
            )

        shapes["centerCross"] = (
            shapes["centerCol"] + shapes["centerRow"] - shapes["center"]
        )

        shapes["outerRing"] = lambda w=1: np.pad(
            np.zeros((self.height - 2 * int(w), self.width - 2 * int(w))),
            ((int(w), int(w)), (int(w), int(w))),
            "constant",
            constant_values=((1, 1), (1, 1)),
        )

        self.shapes = shapes

    def set_shape(self, shape, color, overwrite, shape_args=[]):
        shape_matrix = (
            self.shapes[shape]
            if len(self.supported_shapes[shape]) == 0
            else self.shapes[shape](*list(filter(lambda x: x != "", shape_args)))
        )
        state_matrix = np.reshape(shape_matrix, (self.height, self.width, 1)) * color
        self.set_state_matrix(state_matrix, overwrite)

    def set_shape_LUT(self, shape, grid_lut, overwrite, shape_args=[]):
        shape_matrix = (
            self.shapes[shape]
            if len(self.supported_shapes[shape]) == 0
            else self.shapes[shape](*list(filter(lambda x: x != "", shape_args)))
        )
        state_matrix = np.reshape(shape_matrix, (self.height, self.width, 1)) * grid_lut
        self.set_state_matrix(state_matrix, overwrite)

    def set_state_matrix(self, state_matrix, overwrite=True):
        if overwrite:
            self.state_matrix = state_matrix
        else:
            self.state_matrix += state_matrix

        self.write_to_led()

    def set_pixel(self, pixel_y, pixel_x, colour):
        self.state_matrix[pixel_y, pixel_x] = colour
        self.write_to_led()

    def write_to_led(self):
        return
