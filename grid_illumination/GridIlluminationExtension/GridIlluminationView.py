import importlib.resources as pkg_resources
from collections import OrderedDict

import numpy as np
from flask import make_response
from jinja2 import Template
from labthings import fields, find_component, find_extension
from labthings.schema import Schema
from labthings.views import ActionView, PropertyView, View

from . import templates


class GridIlluminationSetShapeView(ActionView):
    args = {
        "shape": fields.String(required=True),
        "red": fields.Integer(missing=0, allow_none=True),
        "green": fields.Integer(missing=0, allow_none=True),
        "blue": fields.Integer(missing=0, allow_none=True),
        "overwrite": fields.Boolean(missing=True),
        "custom": fields.String(missing="", allow_none=True),
        "saveColor": fields.Boolean(missing=True, allow_none=True),
        "paramValues": fields.List(fields.String, missing=[], allow_none=True),
    }

    def post(self, args):
        shape = args.get("shape")
        color = np.array(
            [args.get("red") or 0, args.get("green") or 0, args.get("blue") or 0]
        )
        overwrite = args.get("overwrite")

        grid_illumination_extension = find_extension(
            "org.openflexure.grid-illumination"
        )
        grid_illumination_extension.led_grid.set_shape(
            shape, color, overwrite, args["paramValues"]
        )
        if args["saveColor"]:
            grid_illumination_extension.save_color_picker(
                args["red"], args["green"], args["blue"]
            )


class GridIlluminationMatrixView(ActionView):
    args = {
        "pixel_x": fields.Integer(missing=-1),
        "pixel_y": fields.Integer(missing=-1),
        "red": fields.Integer(missing=-1),
        "green": fields.Integer(missing=-1),
        "blue": fields.Integer(missing=-1),
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Set the default representations
        def output_html(data, code, headers=None):
            resp = make_response(data, code)
            resp.headers.extend(headers or {})
            resp.mimetype = "text/html"
            return resp

        self.representations = OrderedDict({"text/html": output_html})

    def get(self):
        """
        Returns HTML form used to control the matrix, showing current status
        """
        grid_illumination_extension = find_extension(
            "org.openflexure.grid-illumination"
        )
        templateText = pkg_resources.read_text(templates, "matrixForm.html")
        template = Template(templateText)

        return template.render(
            {
                "initialGrid": grid_illumination_extension.led_grid.state_matrix.tolist(),
                "initialRed": grid_illumination_extension.color_picker["red"],
                "initialBlue": grid_illumination_extension.color_picker["blue"],
                "initialGreen": grid_illumination_extension.color_picker["green"],
                "supportedShapes": grid_illumination_extension.led_grid.supported_shapes,
            }
        )

    def post(self, args):
        grid_illumination_extension = find_extension(
            "org.openflexure.grid-illumination"
        )
        grid_illumination_extension.led_grid.set_pixel(
            args["pixel_y"],
            args["pixel_x"],
            np.array([args["red"], args["green"], args["blue"]]),
        )
        grid_illumination_extension.save_color_picker(
            args["red"], args["green"], args["blue"]
        )
